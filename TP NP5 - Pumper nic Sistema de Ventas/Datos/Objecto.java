package Datos;

public class Objecto {
	private String _nombre;
	private double _cantidad;
	private String opciones;
	
	///Contructores
	public Objecto(String _nombre, double _cantidad) {
		this._nombre = _nombre;
		this._cantidad = _cantidad;
		this.opciones = "";
	}
	public Objecto(String _nombre) {
		this._nombre = _nombre;
		this._cantidad = 1;
		this.opciones = "";
	}
	public Objecto() {
		// TODO Auto-generated constructor stub
		this._nombre =  "";
		this._cantidad = 1;
		this.opciones = "";
	}
	
	public Objecto(String nombre, String opciones) {
		// TODO Auto-generated constructor stub
		this._nombre = nombre;
		this._cantidad = 1;
		this.opciones = opciones;
	}
	public String getOpciones() {
		return opciones;
	}
	public String getNombre() {
		return _nombre;
	}
	public double getCantidad() {
		return _cantidad;
	}

	public void setCantidad(double d) {
		this._cantidad = d;
	}
	public void setOpciones(String opciones) {
		this.opciones = opciones;
	}
	public void setNombre(String name) {
		this._nombre = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Objecto [_nombre:" + _nombre + ", _cantidad:" + _cantidad + ", opciones:" + opciones + "]";
	}
	
	

}
