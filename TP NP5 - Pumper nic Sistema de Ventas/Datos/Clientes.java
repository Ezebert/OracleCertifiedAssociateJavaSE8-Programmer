package Datos;

import java.util.Date;

import Models.DAClientes;

public class Clientes {
	private String _nombre;
	private String _apellido;
	private Date _fecha;
	private long _telefono;
	private boolean ticket;

	public Clientes(String nombre, String apellido, Date fecha, long telefono) {
		_nombre = nombre;
		this._apellido = apellido;
		_fecha = fecha;
		this._telefono = telefono;
		this.ticket = false;
	}

	public Clientes(String _nombre, String _apellido, Date _fecha, long _telefono, boolean ticket) {
		this._nombre = _nombre;
		this._apellido = _apellido;
		this._fecha = _fecha;
		this._telefono = _telefono;
		this.ticket = ticket;
	}

	public String getNombre() {
		return _nombre;
	}

	public String getApellido() {
		return _apellido;
	}

	public Date getFecha() {
		return _fecha;
	}

	public long getTelefono() {
		return _telefono;
	}

	public boolean getTicket() {
		return ticket;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public void setApellido(String _apellido) {
		this._apellido = _apellido;
	}

	public void setFecha(Date fecha) {
		_fecha = fecha;
	}

	public void setTelefono(long telefono) {
		this._telefono = telefono;
	}

	public void setTicket(boolean ticket) {
		this.ticket = ticket;
	}

	@Override
	public String toString() {
		return  getNombre() + " " + getApellido() + " | " + getFecha() + " | TEL: " + getTelefono()+ " | "
				+ getTicket() + " "+DAClientes.getPorcetaje()+"\n";
				//+ (!getTicket() ? " " : "Ticket") + " NOLA\n";
	}

}
