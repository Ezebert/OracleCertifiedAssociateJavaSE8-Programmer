package Datos;

public class Ventas {
	Combo combo;
	double price;
	public Ventas(Combo combo, double price) {
		this.combo = combo;
		this.price = price;
	}
	@Override
	public String toString() {
		return " { " +combo.getNombre() + "     \t price :" + price + " }";
	}
	public Combo getCombo() {
		return combo;
	}
	public double getPrice() {
		return price;
	}
	public void setCombo(Combo combo) {
		this.combo = combo;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	

}
