package Datos;

import java.util.ArrayList;

public class Combo {
	private String _nombre;
	private double _cost;
	private ArrayList<Objecto> _food;
	private Objecto _toys;
	private Objecto _drinks;
	private Objecto _frenys;
	private Objecto gini;
	private Objecto mobur;

	public Combo(String nombre, double cost) {
		// TODO Auto-generated constructor stub
		this._cost = cost;
		this._nombre = nombre;
		_food = new ArrayList<Objecto>();
		_toys = new Objecto((int) (Math.random() * 10) > 5 ? "Varon" : "Mujer");
		_drinks = new Objecto();
		_frenys = new Objecto();
		gini = new Objecto("Ginis", 0);
		mobur = new Objecto("Mobur", 0);
	}

	public void addFood(Objecto food) {
		_food.add(food);
	}

	public void removeFood(Objecto food) {
		_food.remove(food);
	}

	public Objecto getDrinks() {
		return _drinks;
	}
	public ArrayList<Objecto> getFood() {
		return _food;
	}
	public Objecto getFrenys() {
		return _frenys;
	}
	public Objecto getToys() {
		return _toys;
	}
	public double getCost() {
		return _cost;
	}
	public String getNombre() {
		return _nombre;
	}
	public Objecto getGini() {		return gini;	}

	public Objecto getMobur() {		return mobur;	}

	public void setFood(ArrayList<Objecto> _food) {		this._food = _food;	}

	public void setDrinks(Objecto _drinks) {		this._drinks = _drinks;	}

	public void setFrenys(Objecto _frenys) {		this._frenys = _frenys;	}

	public void setCost(double cost) {		this._cost = cost;	}

	public void setNombre(String nombre) {		this._nombre = nombre;	}

	public void setGini(Objecto gini) {		this.gini = gini;	}

	public void setMobur(Objecto mobur) {		this.mobur = mobur;	}

	@Override
	public String toString() {		String ret = " ";		ret += "\t" + this.getNombre() + "\t" + this.getCost() + "\n";
		for (Objecto objeto : this.getFood())
			ret += (ret.length() > 0 ? ", " : "") + objeto.getNombre()
					+ (objeto.getCantidad() > 1 ? " X " + objeto.getCantidad() : " ")
					+ (objeto.getOpciones().length() > 0 ? objeto.getOpciones() : " ");
		ret += "[Frenys " + this.getFrenys().getCantidad() + "]";
		ret += "[Toys " + this.getToys().getOpciones() + "]";
		ret += "[" + this.getDrinks().getNombre() + " " + this.getDrinks().getOpciones() + "]";
		ret += (this.getGini().getCantidad() > 0 ? "[Ginis " + this.getGini().getCantidad() + " ]" : " ");
		ret += (this.getMobur().getCantidad() > 0 ? "[Mobur " + this.getMobur().getCantidad() + " ]" : " ");

		ret += "\n";

		return "{ " + ret + " }";
	}

}
