package Datos;

import java.util.ArrayList;

public class Stock {
	private ArrayList<Objecto> _food = new ArrayList<Objecto>();
	private ArrayList<Objecto> _drinks = new ArrayList<Objecto>();
	private Objecto _frenys = new Objecto();
	private Objecto _ginis = new Objecto();
	private Objecto _mobur = new Objecto();
	private ArrayList<Objecto> _toys = new ArrayList<Objecto>();

	public Objecto getMobur() {
		return _mobur;
	}

	public void setMobur(Objecto mobur) {
		this._mobur = mobur;
	}

	

	public ArrayList<Objecto> getFood() {
		return _food;
	}

	public ArrayList<Objecto> getDrinks() {
		return _drinks;
	}

	public Objecto getFrenys() {
		return _frenys;
	}

	public Objecto getGinis() {
		return _ginis;
	}

	public ArrayList<Objecto> getToys() {
		return _toys;
	}

	public void setFood(ArrayList<Objecto> food) {
		this._food = food;
	}

	public void setDrinks(ArrayList<Objecto> drinks) {
		this._drinks = drinks;
	}

	public void setFrenys(Objecto frenys) {
		this._frenys = frenys;
	}

	public void setGinis(Objecto ginis) {
		this._ginis = ginis;
	}

	public void setToys(ArrayList<Objecto> toys) {
		this._toys = toys;
	}

	@Override
	public String toString() {
		return "Stock [food:" + _food + ", drinks:" + _drinks + ", frenys:" + _frenys + ", toys:" + _toys + "]";
	}

	/*
	 * Food hambuerguesa; Food queso; Food aderesos; Food panConCenteno; Food
	 * panSINCenteno; Drinks Gaseosa; Drinks Cerveza; Drinks aguaSinGas; Frenys
	 * frenys; Toys toys;
	 */

}
