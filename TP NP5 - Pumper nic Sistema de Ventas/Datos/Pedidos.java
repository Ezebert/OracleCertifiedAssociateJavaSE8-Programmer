package Datos;

import Models.DAClientes;

public class Pedidos implements Runnable {
	private Ventas ventas;

	private long initialTime;

	public Pedidos(Ventas ventas, long initialTime) {
		this.ventas = ventas;
		this.initialTime = initialTime;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		long id = Thread.currentThread().getId();
		Thread th = Thread.currentThread();

		System.out.println("--->>> START   | PUESTO " + id + " | ORDEN -" + this.ventas.getCombo().getNombre()
				+ " | TIEMPO: " + (System.currentTimeMillis() - this.initialTime) / 1000 + "seg");
		wait(.5);

		System.out.println("FINISH <<<<--- | PUESTO " + id + "|\t TIEMPO: "
				+ (System.currentTimeMillis() - this.initialTime) / 1000 + "seg");
		// Ticket - Guardamos Datos CLientes
		if (id == 11 || id == 10) {
			DAClientes.addCliente("Cliente", "Nro: " + DAClientes.tama�o(), new java.util.Date(), 1, true);
			Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
		} else if (DAClientes.getPorcetaje() <= 60) {

			try {
				th.join(5);
				if (!th.isAlive() || th.isDaemon())
					DAClientes.addCliente("Cliente", "Nro: " + DAClientes.tama�o(), new java.util.Date(), 1, false);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			DAClientes.addCliente("Cliente", "Nro: " + DAClientes.tama�o(), new java.util.Date(), 1);

		}

	}

	private void wait(double segundos) {
		try {
			Thread.sleep((long) (segundos * 1000));
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

}
