import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Datos.Pedidos;
import Datos.Ventas;
import Models.Admin;
import Models.Archivo;
import Models.lasVentas;
import Models.losCombos;
import Models.losEmpleados;
import Models.losStock;

public class Negocio {

	/**
	 * @param args
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		losCombos.load();
		System.out.println("COMBOS DISPONIBLES");
		System.out.println(losCombos.showAll());
		losStock.load();
		System.out.println("STOCK INICIAL");
		System.out.println(losStock.showAll());
		losEmpleados.puestos(6);
		System.out.print("ingrese la cantidad de ventas: ");
		lasVentas.generate(new Scanner(System.in).nextInt());

		// MULTI TAREA
		long init = System.currentTimeMillis(); // Instante inicial del
												// procesamiento
		ExecutorService executor = Executors.newFixedThreadPool(losEmpleados.cantPuesto());
		for (int i = 0; i < lasVentas.tama�o(); i++) {
			Ventas v = lasVentas.get(i);
			Runnable pedidos = new Pedidos(v, init);
			executor.execute(pedidos);
			if ((i % 30 == 0 && i != 0) || i == lasVentas.tama�o() - 1)
				Archivo.escribirTXT(getNombreFIle());
		}
		executor.shutdown(); // Cierro el Executor
		while (!executor.isTerminated()) {
		}
		System.out.println("STOCK ACTUAL");
		System.out.println(losStock.showAll());
		System.out.println("ADMIN ");
		if(checkDatos())
			Archivo.leerTXt(getNombreFIle());
		else 
			System.out.println("USTED NO ES ADMIN");

	}

	private static boolean checkDatos() {
		// TODO Auto-generated method stub
//		System.out.print("Usuario: ");
//		String users = new Scanner(System.in).next();
//		System.out.print("Password: ");
//		String password = new Scanner(System.in).next();
//		return Admin.check(users,password);
		return Admin.check("01354","frenchisDobles");
	}

	private static String getNombreFIle() {
		// TODO Auto-generated method stub
		Date date = new Date();
		SimpleDateFormat dt = new SimpleDateFormat("ddMMyyyy");
		return dt.format(date);
	}

}
