package Models;

import java.util.ArrayList;
import java.util.Date;

import Datos.Clientes;

public class DAClientes {
	private static ArrayList<Clientes> _listCliente = new ArrayList<Clientes>();

	public static void addCliente(String nombre, String apellido, Date date, int telefono, boolean b) {
		// TODO Auto-generated method stub
		Clientes cli = new Clientes(nombre, apellido, date, telefono, b);
		agregar(cli);

	}

	public static void addCliente(String nombre, String apellido, Date date, int telefono) {
		// TODO Auto-generated method stub
		Clientes cli = new Clientes(nombre, apellido, date, telefono);
		agregar(cli);

	}

	private static void agregar(Clientes cli) {
		// TODO Auto-generated method stub
		_listCliente.add(cli);

	}

	public static String showAll() {
		String ret = "";
		for (Clientes cli : _listCliente) {
			ret += cli.toString();
		}
		return "{ " + ret + " }";
	}

	public static int tama�o() {
		// TODO Auto-generated method stub
		return _listCliente.size();
	}

	public static int cantTIcket() {
		// TODO Auto-generated method stub
		int ret = 0;

		for (Clientes clientes : _listCliente) {
			if (clientes.getTicket())
				ret++;
		}
		return ret;
	}

	public static double getPorcetaje() {
		// TODO Auto-generated method stu
		if (tama�o() != 0) {
			return cantTIcket() * 100 / tama�o();
		}

		return 0;
	}

}
