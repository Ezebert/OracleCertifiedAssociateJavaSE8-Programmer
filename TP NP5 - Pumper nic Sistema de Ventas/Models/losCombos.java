package Models;

import java.util.ArrayList;

import Datos.Combo;
import Datos.Objecto;

public class losCombos {
	public static ArrayList<Combo> _listCombos = new ArrayList<Combo>();

	public static void load() {
		// TODO Auto-generated method stub
		addCombo("Pumper Felix", 1000);
		addCombo("Pumper Mediano", 2000);
		addCombo("Pumper Grande", 4000);
		addCombo("Super Pumper", 5000);
		
		setCombos();
	}

	private static void setCombos() {
		// TODO Auto-generated method stub
		_listCombos.get(0).addFood(new Objecto("Hambuerguesa"));
		_listCombos.get(0).addFood(new Objecto("Queso"));
		_listCombos.get(0).addFood(new Objecto("Aderesos"));
		_listCombos.get(0).addFood(new Objecto("Pan", (((int) (Math.random()*2)) == 0 ? " C/Centeno" : " S/Centeno")));
		String bebida = (int) (Math.random() * 2) == 0 ? " Gaseosa " : " Agua S/gas ";
		_listCombos.get(0).setDrinks(new Objecto(bebida, "pequeño"));
		_listCombos.get(0).setFrenys(new Objecto(" "));
		
		_listCombos.get(1).addFood(new Objecto("Hambuerguesa",2));
		_listCombos.get(1).addFood(new Objecto("Queso",2));
		_listCombos.get(1).addFood(new Objecto("Aderesos"));
		_listCombos.get(1).addFood(new Objecto("Pan", (((int) (Math.random()*2)) == 0 ? " C/Centeno" : " S/Centeno")));
		_listCombos.get(1).setDrinks(new Objecto("Gaseosa", "Mediana"));
		_listCombos.get(1).setFrenys(new Objecto("Mediano",1));
		
		_listCombos.get(2).addFood(new Objecto("Hambuerguesa",3));
		_listCombos.get(2).addFood(new Objecto("Queso",4));
		_listCombos.get(2).addFood(new Objecto("Aderesos"));
		_listCombos.get(2).addFood(new Objecto("Pan", (((int) (Math.random()*2)) == 0 ? " C/Centeno" : " S/Centeno")));
		String tamañoBebida = (int) (Math.random() * 2) == 0 ? "Grande" : "Mediana";
		_listCombos.get(2).setDrinks(new Objecto("Gaseosa", tamañoBebida));
		_listCombos.get(2).setFrenys(new Objecto("Grande",2));
		_listCombos.get(2).getGini().setCantidad(1);
		
		_listCombos.get(3).addFood(new Objecto("Hambuerguesa",3));
		_listCombos.get(3).addFood(new Objecto("Queso",4));
		_listCombos.get(3).addFood(new Objecto("Aderesos"));
		_listCombos.get(3).addFood(new Objecto("Pan", (((int) (Math.random()*2)) == 0 ? " C/Centeno" : " S/Centeno")));
		bebida = (int) (Math.random() * 2) == 0 ? "Gaseosa" : "Cerveza";
		_listCombos.get(3).setDrinks(new Objecto(bebida, "Grande"));
		_listCombos.get(3).setFrenys(new Objecto("Super",3));
		_listCombos.get(3).getGini().setCantidad(2);
		_listCombos.get(3).getMobur().setCantidad(1);
	}

	public static void addCombo(String name, int cost) {
		// TODO Auto-generated method stub
		_listCombos.add(new Combo(name, cost));

	}

	public static String showAll() {
		// TODO Auto-generated method stub
		String ret = "";
		for (Combo combo : _listCombos) {
			ret += combo.toString();	
		}
		return "{ " + ret + " }";
		
	}

	public static Combo get(int i) {
		// TODO Auto-generated method stub
		return _listCombos.get(i);
	}

	public static int getTamaño() {
		// TODO Auto-generated method stub
		return _listCombos.size();
	}

}
