package Models;

import java.util.ArrayList;

import Datos.Combo;
import Datos.Objecto;
import Datos.Stock;

public class losStock {
	private static Stock _stock = new Stock();

	public static void load() {
		// TODO Auto-generated method stub
		addFood("Hambuerguesa", 100);
		addFood("Queso", 300);
		addFood("Pan C/Centeno", 70);
		addFood("Pan S/Centeno", 70);
		addFood("Aderesos", 70);

		addDrinks("Agua s/gas", 120);
		addDrinks("Gaseosa", 120);
		addDrinks("Cerveza", 120);

		addToys("Toy Varones", 100);
		addToys("Toy Mujeres", 100);

		addFrenchys("Frenchys", 1000);

		addGinis("Ginis", 70);
		addMobur("Mobur", 70);
	}

	public static Stock getStock() {
		return _stock;
	}

	public static void setStock(Stock _stock) {
		losStock._stock = _stock;
	}

	private static void addDrinks(String string, int i) {
		// TODO Auto-generated method stub
		_stock.getDrinks().add(new Objecto(string, i*12));
	}

	private static void addGinis(String string, int i) {
		// TODO Auto-generated method stub
		_stock.setGinis(new Objecto(string, i));
	}

	private static void addMobur(String string, int i) {
		// TODO Auto-generated method stub
		_stock.setMobur(new Objecto(string, i));
	}

	private static void addToys(String string, int i) {
		// TODO Auto-generated method stub
		_stock.getToys().add(new Objecto(string, i));
	}

	private static void addFrenchys(String string, int i) {
		// TODO Auto-generated method stub
		_stock.setFrenys(new Objecto(string, i));
		_stock.getFrenys().setNombre("Frenchys");

	}

	private static void addFood(String name, int amount) {
		// TODO Auto-generated method stub
		_stock.getFood().add(new Objecto(name, amount*10));
	}

	public static String showAll() {
		// TODO Auto-generated method stub
		String ret = " ITEM \t\t CANTIDAD \n";
		for (Objecto f : _stock.getFood())
			ret += "(" + f.getCantidad() + " Unid)\t" + f.getNombre() + "\n";
		ret += "----------------------------------\n";
		for (Objecto d : _stock.getDrinks())
			ret += "(" + d.getCantidad() + " Unid)\t" + d.getNombre() + "\n";
		ret += "----------------------------------\n";
		ret += "(" + _stock.getFrenys().getCantidad() + " kg)\t" + _stock.getFrenys().getNombre() + "\n";
		ret += "(" + _stock.getGinis().getCantidad() + " Unid)\t" + _stock.getGinis().getNombre() + "\n";
		ret += "(" + _stock.getMobur().getCantidad() + " Unid)\t" + _stock.getMobur().getNombre() + "\n";
		ret += "----------------------------------\n";
		for (Objecto t : _stock.getToys())
			ret += "(" + t.getCantidad() + " Unid)\t" + t.getNombre() + "\n";
		return ret;

	}

	private static void check(int min) {
		// TODO Auto-generated method stub
		for (Objecto f : _stock.getFood())
			if (f.getCantidad() < min)
				throw new Error("[" + f.getNombre() + "] Stock Bajo");
		for (Objecto t : _stock.getToys())
			if (t.getCantidad() < min)
				throw new Error("[" + t.getNombre() + "] Stock Bajo");
		if (_stock.getFrenys().getCantidad() < min)
			throw new Error("[" + _stock.getFrenys().getNombre() + "] Stock Bajo");
		if (_stock.getGinis().getCantidad() < min)
			throw new Error("[" + _stock.getGinis().getNombre() + "] Stock Bajo");
	}

	public static void setStock(Combo combo) {
		// TODO Auto-generated method stub
		// c.getFood()
		check(50);
		checkFood(combo);
		// c.getDrinks()
		checkDrinks(combo);
		// c.getFrenys()
		checkFrenys(combo);
		// c.getMobur()
		checkMobur(combo);
		// c.getGini()
		checkGinis(combo);
		// c.getToys()
		checkToys(combo);

	}

	public static void checkToys(Combo combo) {
		double inventario;
		inventario = combo.getToys().getCantidad();
		ArrayList<Objecto> t = _stock.getToys();
		if (combo.getToys().getOpciones().equals("Varon")) {
			t.get(0).setCantidad(t.get(0).getCantidad() - inventario);
		} else { // Mujer
			t.get(1).setCantidad(t.get(1).getCantidad() - inventario);
		}
	}

	public static void checkGinis(Combo combo) {
		double inventario;
		Objecto g = _stock.getGinis();
		inventario = combo.getGini().getCantidad();
		g.setCantidad(g.getCantidad() - inventario);
	}

	public static void checkMobur(Combo combo) {
		double inventario;
		Objecto m = _stock.getMobur();
		inventario = combo.getMobur().getCantidad();
		m.setCantidad(m.getCantidad() - inventario);
	}

	public static void checkFrenys(Combo combo) {
		double inventario;
		Objecto f = _stock.getFrenys();
		inventario = combo.getFrenys().getCantidad();
		f.setCantidad(f.getCantidad() - inventario);
	}

	public static void checkDrinks(Combo combo) {
		double inventario;
		for (Objecto d : _stock.getDrinks()) {
			if (d.getNombre().equals(combo.getDrinks().getNombre())) {
				inventario = combo.getDrinks().getCantidad();
				d.setCantidad(d.getCantidad() - inventario);
				break; // Corto el ciclo
			}
		}
	}

	public static void checkFood(Combo combo) {
		for (Objecto comb : combo.getFood()) {
			for (Objecto stock : _stock.getFood()) {
				if (stock.getNombre().equals(combo.getNombre())) {
					stock.setCantidad(stock.getCantidad() - comb.getCantidad());
					break;
				} else if (combo.getNombre().concat(comb.getOpciones()).equals(stock.getNombre())) {
					stock.setCantidad(stock.getCantidad() - comb.getCantidad());
					break;
				}
			}
		}
	}

}
