package Models;

import java.util.ArrayList;

import Datos.Combo;
import Datos.Ventas;

public class lasVentas {
	private static ArrayList<Ventas> _ventas = new ArrayList<Ventas>();

	public static void generate(int cantVentas) {
		// TODO Auto-generated method stub
		for (int j = 0; j < cantVentas; j++) {
			Combo c = losCombos.get((int) (Math.random() * losCombos.getTama�o()));
			agregar(c, c.getCost());
			losStock.setStock(c);
		}

	}

	public static void agregar(Combo c, double price) {
		// TODO Auto-generated method stub
		_ventas.add(new Ventas(c, price));

	}

	public static ArrayList<Ventas> getVentas() {
		// TODO Auto-generated method stub
		return _ventas;
	}

	public static double sumSold() {
		// TODO Auto-generated method stub
		double ret = 0.00f;
		for (Ventas ventas : _ventas) {
			ret += ventas.getPrice();
		}
		return ret;
	}

	public static int tama�o() {
		// TODO Auto-generated method stub
		return _ventas.size();
	}

	public static Ventas get(int index) {
		// TODO Auto-generated method stub
		return _ventas.get(index);
	}

}
