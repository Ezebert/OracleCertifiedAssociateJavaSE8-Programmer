import java.util.ArrayList;

public class LasCategorias {
	private static ArrayList<Categoria> _categorias = new ArrayList<Categoria>();

	static public void agregarCategorias() {
		agregar("Mosca ", 48.988, 50.802);
		agregar("Gallo ", 52.163, 53.525);
		agregar("Pluma ", 55.338, 57.152);
		agregar("Ligero ", 58.967, 61.237);
		agregar("Welter ", 63.503, 66.678);
		agregar("Mediano ", 69.853, 72.562);
		agregar("Mediopesado ", 76.205, 79.378);
		agregar("Pesado ", 91.0000, 99);

	}

	static public void agregar(String nombreCategoria, double min, double max) {
		_categorias.add(new Categoria(nombreCategoria, min, max));
	}

	public static String asignar(double peso) {
		// TODO Auto-generated method stub
		if(peso<_categorias.get(0).getMinimo()) //minimo de minimo
			return _categorias.get(0).getCategoria();
		
		if(peso>=_categorias.get(tama�o()-1).getMinimo()) //maximo de maximo
			return _categorias.get(tama�o()-1).getCategoria();
				
		for (int index = 0; index < tama�o()-1; index++) { //No tiene en cuenta el MAX = tope
			Categoria cat = _categorias.get(index);
			if(peso>=cat.getMinimo() && peso<=cat.getMaximo())
				return cat.getCategoria();
			if(peso<_categorias.get(index+1).getMinimo() && index< tama�o())
				return cat.getCategoria();
		}
		 throw new IllegalArgumentException("No existe categoria para peso: "+peso);
	}

	private static int tama�o() {
		// TODO Auto-generated method stub
		
		return _categorias.size();
	}

	public static int index(String nombre) {
		// TODO Auto-generated method stub
		int ret  = 0;
		for (int i = 0; i < tama�o();i = i+2) {
			Categoria cat = _categorias.get(i);
			Categoria cat2 = _categorias.get(i+1);
			if(cat.getCategoria().equals(nombre)||cat2.getCategoria().equals(nombre))
				return ret;
			ret++;
		}
		throw new IllegalArgumentException("No existe la categoria: "+nombre);
	}
}
