
public class Boxeador {
//	private final static String tipoCategoria[] = { "Mosca ", "Gallo ", "Pluma " ,"Ligero ","Welter ","Mediano ","Mediopesado ","Pesado "};
	private String categoria;
	private double peso;
	/*
	 * Mosca 48.988 -- 50.802 
	 * Gallo 52.163 --53.525 
	 * Pluma 55.338 -- 57.152 *
	 * Ligero 58.967 -- 61.237
	 *  Welter 63.503 -- 66.678 
	 *  Mediano 69.853 -- 72.562 
	 *  Mediopesado 76.205 -- 79.378
	 *   Pesado 91 Sin L�mite
	 */
	
	protected Boxeador(double peso, String categoria) {
		this.peso = peso;
		this.categoria = categoria;	}

	public String getCategoria() {
		return categoria;
	}

	public double getPeso() {
		return peso;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	@Override
	public String toString() {
		return "\n\t["+categoria+":"+ peso +"]";
	}
	
	
	

}
