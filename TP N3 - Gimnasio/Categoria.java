
public class Categoria {
	private String categoria;
	private double minimo;
	private double maximo;
	
	
	protected Categoria(String categoria, double minimo, double maximo) {
		this.categoria = categoria;
		this.minimo = minimo;
		this.maximo = maximo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	

	public double getMinimo() {
		return minimo;
	}

	public double getMaximo() {
		return maximo;
	}

	public void setMinimo(int minimo) {
		this.minimo = minimo;
	}

	public void setMaximo(int maximo) {
		this.maximo = maximo;
	}

	@Override
	public String toString() {
		return " categoria:" + categoria + " ";
	}
	

}
