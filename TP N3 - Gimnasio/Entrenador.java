import java.util.ArrayList;

public class Entrenador {
	private ArrayList<ArrayList<Boxeador>> _boxeador;

	protected Entrenador(int num) {
		_boxeador = new ArrayList<ArrayList<Boxeador>>();
		for (int i = 0; i < num; i++) {
			_boxeador.add(new ArrayList<Boxeador>());
		}
		// TODO Auto-generated constructor stub
	}

	public void agregarBoxeador(double peso) {
		Boxeador box = new Boxeador(peso, LasCategorias.asignar(peso));
		int index =LasCategorias.index(box.getCategoria());
		if(_boxeador.get(index).size()<5)
			_boxeador.get(index).add(box);
		else 
			System.err.println("Categoria "+box.getCategoria()+" = FULL");

	}

	public ArrayList<ArrayList<Boxeador>> get_boxeador() {
		return _boxeador;
	}

	public int totalBoxeador() {
		// TODO Auto-generated method stub
		int ret = 0;
		for (int i = 0; i < 4; i++)
			ret += _boxeador.get(i).size();

		return ret;
	}

	public String informe() {
		String ret = "";
		int cont = 0;
		for (int i = 0; i < _boxeador.size(); i++) {
			cont +=_boxeador.get(i).size();
			ret+="\nEntrenador Nro "+ (i+1)+" tiene "+_boxeador.get(i).size()+" boxeador(s)\n";
			ret += _boxeador.get(i).toString() + "\n";
		}
		System.out.println("Total Boxeadores: "+cont);
		return ret;
	}
	public int asignarEntrenador(Boxeador box) {
		// TODO Auto-generated method stub
		switch (box.getCategoria()) {
		case "Mosca":		case "Gallo": 			return 0;	
		case "Pluma":		case "Ligero": 			return 1; 
		case "Welter":		case "Mediano":			return 2; 
		case "MedioPesado":	case "Pesado": 			return 3; 
		default: return -1; 
			}
		
	}

}
