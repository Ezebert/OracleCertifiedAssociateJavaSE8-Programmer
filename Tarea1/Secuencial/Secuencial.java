package Secuencial;

import java.util.Scanner;

public class Secuencial {
	private Scanner sc = new Scanner(System.in);

	public void mostrar(int i) {
		// TODO Auto-generated method stub
		System.out.println("\nEjercicios Básicos con Estructura Secuencial: " + i);
		switch (i) {
		case 1:
			ejercicosNro1();
			break;
		case 2:
			ejercicosNro2();
			break;
		case 3:
			ejercicosNro3();
			break;
		case 4:
			ejercicosNro4();
			break;
		case 5:
			ejercicosNro5();
			break;
		case 6:
			ejercicosNro6();
			break;
		case 7:
			ejercicosNro7();
			break;
		case 8:
			ejercicosNro8();
			break;
		case 9:
			ejercicosNro9();
			break;
		case 10:
			ejercicosNro10();
			break;
		case 11:
			ejercicosNro11();
			break;
		case 12:
			ejercicosNro12();
			break;
		case 13:
			ejercicosNro13();
			break;
		default:
			System.err.println("\nNro de ejercicos no requerido");
			break;
		}
	}

	private void ejercicosNro13() {
		// TODO Auto-generated method stub

	}

	private void ejercicosNro12() {
		// TODO Auto-generated method stub

	}

	private void ejercicosNro11() {
		// TODO Auto-generated method stub

	}

	private void ejercicosNro10() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese un nro de tres cifras: ");
		String numero = sc.next();
		for (int i = 0; i < numero.length(); i++) {
			System.out.print(numero.charAt(i)+" ");
		}
		

	}

	private void ejercicosNro9() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese altura:  ");
		double altura = sc.nextDouble();
		System.out.print("Ingrese base ");
		double base = sc.nextDouble();
		System.out.println("Area del triangulo: "+ (base+altura)/2);

	}

	private void ejercicosNro8() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese radio de la circuferencia: ");
		double radio = sc.nextDouble();
//		4/3 · π · radio 3
		System.out.println("EL volumen de una esfera: "+(4 * Math.PI * Math.pow(radio, 3)) / 3);
	}

	private void ejercicosNro7() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese cateto A: ");
		double cateto1 = sc.nextDouble();
		System.out.print("Ingrese cateto B: ");
		double cateto2 = sc.nextDouble();
		// Longitud = hipotenusa ?
		System.out.println("La longitud es: " + Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2)));
		System.err.println(Math.hypot(cateto1, cateto2));
	}

	private void ejercicosNro6() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese la cantidad de Kilometros a pasar a metros: ");
		double kilometros = sc.nextDouble();
		System.out.println("metros: " + kilometros * 1000);
	}

	private void ejercicosNro5() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese radio de la circuferencia: ");
		double radio = sc.nextDouble();
		System.out.println("Longitud: " + 2 * Math.PI * radio + " Circunferencia: " + Math.PI * Math.pow(radio, 2));
	}

	private void ejercicosNro4() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese cantidad de grados centigrados: ");
		double grados = sc.nextDouble();
		System.out.println("Grados Fahrenheit: " + (32 + (9 * grados / 5)));
	}

	private void ejercicosNro3() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese primer nro: ");
		int nro = sc.nextInt();
		System.out.println("El doble de sus valor es: " + nro * 2 + " y su triple es:" + nro * 3);
	}

	private void ejercicosNro2() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese un nombre: ");
		String nombre = sc.nextLine();
		System.out.println("Buenos dias " + nombre);
	}

	private void ejercicosNro1() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese primer nro: ");
		int nro1 = sc.nextInt();
		System.out.print("Ingresa otro numero: ");
		int nro2 = sc.nextInt();
		System.out.println("Los numeros ingresados son: " + nro1 + " y " + nro2);
	}

}
