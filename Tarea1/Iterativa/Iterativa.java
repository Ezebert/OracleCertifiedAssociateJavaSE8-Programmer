package Iterativa;

public class Iterativa {

	public void mostrar(int i) {
		// TODO Auto-generated method stub
		System.out.println("\nEjercicios B�sicos con Estructura Iterativa o Repetitiva:" + i);
		switch (i) {
		case 1:
			ejercicosNro1();
			break;
		case 2:
			ejercicosNro2();
			break;
		case 3:
			ejercicosNro3();
			break;
		case 4:
			ejercicosNro4();
			break;
		case 5:
			ejercicosNro5();
			break;
		case 6:
			ejercicosNro6();
			break;
		default:
			System.err.println("\nNro de ejercicos no requerido");
			break;
		}
	}

	private void ejercicosNro6() {
		// TODO Auto-generated method stub
		for (int i = 100; i > 0; i--) {
			System.out.print(i + " ");
		}
	}

	private void ejercicosNro5() {
		// TODO Auto-generated method stub
		int i = 100;
		do {
			System.out.print(i-- + " ");
		} while (i != 0);
	}

	private void ejercicosNro4() {
		// TODO Auto-generated method stub
		int i = 100;
		while (i != 0) {
			System.out.print(i-- + " ");
		}
	}

	private void ejercicosNro3() {
		// TODO Auto-generated method stub
		for (int i = 1; i <= 100; i++) {
			System.out.print(i + " ");
		}
	}

	private void ejercicosNro1() {
		// TODO Auto-generated method stub
		int i = 0;
		while (i < 100) {
			System.out.print(++i + " ");
		}
	}

	private void ejercicosNro2() {
		// TODO Auto-generated method stub
		int i = 0;
		do {
			System.out.print(++i + " ");
		} while (i < 100);
	}

}
