package Condicional;

import java.util.Scanner;

public class Condicional {

	private Scanner sc = new Scanner(System.in);

	public void mostrar(int i) {
		// TODO Auto-generated method stub
		System.out.println("\nEjercicios B�sicos con Estructura Condicional: " + i);
		switch (i) {
		case 1:
			ejercicosNro1();
			break;
		case 2:
			ejercicosNro2();
			break;
		case 3:
			ejercicosNro3();
			break;
		case 4:
			ejercicosNro4();
			break;
		case 5:
			ejercicosNro5();
			break;
		case 6:
			ejercicosNro6();
			break;
		case 7:
			ejercicosNro7();
			break;
		case 8:
			ejercicosNro8();
			break;
		case 9:
			ejercicosNro9();
			break;
		case 10:
			ejercicosNro10();
			break;

		default:
			System.err.println("\nNro de ejercicos no requerido");
			break;
		}
	}

	private void ejercicosNro10() {
		// TODO Auto-generated method stub
		System.out.print("Ingresa un numero del mes: ");
		int mes = sc.nextInt();
		switch (mes) {
		case 2:
			System.out.println("El mes " + mes + " tiene 28 dias");
			break;
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.println("El mes " + mes + " tiene 31 dias");
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println("El mes " + mes + " tiene 30 dias");
			break;
		default:
			System.err.println("El mes " + mes + " no es valido");
			break;
		}

	}

	private void ejercicosNro9() {
		// TODO Auto-generated method stub
		System.out.print("Ingresa un numero: ");
		int a = sc.nextInt();
		System.out.print("Ingresa otro numero: ");
		int b = sc.nextInt();
		System.out.print("Ingresa un numero: ");
		int c = sc.nextInt();
		// Formato 24hs
		if ((a > 0 && a < 24) && (b > 0 || b < 60) && (c > 0 || c < 60))
			System.out.println("Es una hora indica en formato de 24hs " + a + ":" + b + ":" + c);
		else
			System.out.println("[ NO ] pertece a un formato de 24hs");

	}

	private void ejercicosNro8() {
		// TODO Auto-generated method stub
		System.out.print("Ingresa un numero: ");
		int a = sc.nextInt();
		System.out.print("Ingresa otro numero: ");
		int b = sc.nextInt();
		System.out.print("Ingresa un numero: ");
		int c = sc.nextInt();
		if (a > b && a > c) {
			System.out.println("El numero mayor es: " + a);
		} else {
			if (b > a && b > c) {
				System.out.println("El numero mayor es: " + b);
			} else {
				System.out.println("El numero mayor es: " + c);
			}
		}
	}

	private void ejercicosNro7() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese Primer Nro : ");
		double nro = sc.nextDouble();
		System.out.print("Ingrese Segundo Nro : ");
		double divisor = sc.nextDouble();
		if (divisor != 0)
			System.out.println("La divicion entre el nro " + nro + " y " + divisor + " es: " + (nro / divisor));
		else
			System.out.println("[ NO ] es posible dividir por 0 (cero)");

	}

	private void ejercicosNro6() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese un caracter: ");
		char cadena = sc.next().charAt(0);
		if (Character.isDigit(cadena))
			System.out.println("El Caracter " + cadena + " es digito");
		else
			System.out.println("El Caracter " + cadena + " [ NO ] es digito");
	}

	private void ejercicosNro5() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese primer caracter: ");
		char cadena = sc.next().charAt(0);
		System.out.print("Ingrese segundo caracter: ");
		char cadena2 = sc.next().charAt(0);
		if (Character.isLowerCase(cadena) && Character.isLowerCase(cadena2))
			System.out.println("Ambos letras son minusculas");
		else
			System.out.println("Ambos letras [ NO ]son minusculas");
	}

	private void ejercicosNro4() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese primer caracter: ");
		char cadena = sc.next().charAt(0);
		System.out.print("Ingrese segundo caracter: ");
		char cadena2 = sc.next().charAt(0);
		if (cadena == cadena2)
			System.out.println("Ambos caracteres son iguales");
		else
			System.out.println("Ambos caracteres [ NO ] son iguales");
	}

	private void ejercicosNro3() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese un caracter: ");
		char cadena = sc.next().charAt(0);
		if (Character.isUpperCase(cadena))
			System.out.println("El Caracter " + cadena + " es MAYUSCULA");
		else
			System.out.println("El Caracter " + cadena + " es minuscula");

	}

	private void ejercicosNro2() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese Nro entero: ");
		String nro = sc.nextLine();
		if (Integer.parseInt(nro) % 10 == 0)
			System.out.println("El Nro " + nro + " es multiplo de 10");
		else
			System.out.println("El Nro " + nro + "  [ NO ] es multiplo de 10");
	}

	private void ejercicosNro1() {
		// TODO Auto-generated method stub
		System.out.print("Ingrese Nro entero: ");
		String nro = sc.nextLine();
		if (Integer.parseInt(nro) % 2 == 0)
			System.out.println("El Nro " + nro + " es Par");
		else
			System.out.println("El Nro " + nro + " es Impar");
	}
}
