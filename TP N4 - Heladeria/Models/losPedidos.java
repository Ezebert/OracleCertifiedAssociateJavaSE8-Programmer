package Models;
import java.util.ArrayList;

import Data.Envases;
import Data.Gustos;
import Data.Pedidos;
import Data.Plus;

public class losPedidos {
	private static ArrayList<Pedidos> _pedidos = new ArrayList<Pedidos>();

	public static ArrayList<Pedidos> get_pedidos() {
		return _pedidos;
	}

	public static void set_pedidos(ArrayList<Pedidos> _pedidos) {
		losPedidos._pedidos = _pedidos;
	}

	public static String mostrar() {
		// TODO Auto-generated method stub
		String ret = "";
		for (Pedidos pedidos : _pedidos)
			ret += pedidos.toString();
		return ret;
	}

	public static int totales() {
		// TODO Auto-generated method stub
		return _pedidos.size();
	}

	public static void preparar() {
		// TODO Auto-generated method stub
		int random = (int) (Math.random()*losEnvases.total());
		
		Envases  env = losEnvases.getEnvases(random);
		ArrayList<Gustos> _listGustos = losGusto.getGustos(env.getCantGustos());
		Plus plus = losPlus.getGustos(env);
		
		Pedidos pedido = new Pedidos(env.getNombre(),_listGustos, plus); 
		_pedidos.add(pedido);
	}

	public static Pedidos ultimo() {
		// TODO Auto-generated method stub
		return _pedidos.get(totales()-1);
	}
	
//
}
