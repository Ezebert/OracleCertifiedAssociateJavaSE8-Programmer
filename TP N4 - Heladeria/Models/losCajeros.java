package Models;
import java.util.ArrayList;

import Data.Cajeros;

public class losCajeros {
	private static ArrayList<Cajeros> _cajeros = new ArrayList<Cajeros>();
	

	public static ArrayList<Cajeros> get_cajeros() {
		return _cajeros;
	}
	public static void set_cajeros(ArrayList<Cajeros> _cajeros) {
		losCajeros._cajeros = _cajeros;
	}
	
	 
	public static String mostrar() {
		// TODO Auto-generated method stub
		String ret = "";
		int cont = 0;
		for (Cajeros cajeros : _cajeros) {
			cont++;
			ret += "Nro Cajero " + cont +
					cajeros.toString();
		}
		return ret;
	}
	public static void agregarCajeros() {
		// TODO Auto-generated method stub
		agregar(5);
		agregar(10);
		agregar(15);
		
	}
	protected static void agregar(int maxPedidos) {
		// TODO Auto-generated method stub
		_cajeros.add(new Cajeros(maxPedidos));
	}
	
	public static Cajeros asignar() {
		// TODO Auto-generated method stub
		int index = (int) (Math.random()*total());
		Cajeros caja = _cajeros.get(index);
		if(caja.getPedidosHechos()<caja.getMaxPedidos()){
			caja.setPedidosHechos(caja.getPedidosHechos()+1);
			return caja;
		}
		else
			return	buscarCajaLibre();
	}
	private static Cajeros buscarCajaLibre() {
		// TODO Auto-generated method stub
		
		for (Cajeros ret : _cajeros) {
			if(ret.getPedidosHechos()<ret.getMaxPedidos()){
				ret.setPedidosHechos(ret.getPedidosHechos()+1);
				return ret;
			}
		}
		losHeladeros.informe();
		throw new Error("Capidad Maxima de Cajas");
		
		
	}
	private static int total() {
		// TODO Auto-generated method stub
		return _cajeros.size();
	}

}
