package Models;

import java.util.ArrayList;

import Data.Gustos;

public class losGusto {
	private static ArrayList<Gustos> _listGusto = new ArrayList<Gustos>();
	
	public static String mostrar() {
		// TODO Auto-generated method stub
		String ret = "";
		for (Gustos gusto : _listGusto)
			ret += gusto.toString();
		return ret;
	}
	
	public static ArrayList<Gustos> getGustos(int cantGustos) {
		// TODO Auto-generated method stub
		ArrayList<Gustos> ret = new ArrayList<Gustos>();
		for (int i = 0; i < cantGustos; i++) {
			ret.add(buscarGusto());
		}
		return ret;
	}

	private static Gustos buscarGusto() {
		// TODO Auto-generated method stub
		int index = (int) (Math.random()*total());
		return _listGusto.get(index);
	}

	private static int total() {
		// TODO Auto-generated method stub
		return _listGusto.size();
	}

	public static void agregarGustos() {
		// TODO Auto-generated method stub
		agregar("Chocolate");
		agregar("Dulce de Leche");
		agregar("Crema Americana");
		agregar("Frutilla");
	}

	protected static void agregar(String nombre) {
		// TODO Auto-generated method stub
		_listGusto.add(new Gustos(nombre));
	}
}
