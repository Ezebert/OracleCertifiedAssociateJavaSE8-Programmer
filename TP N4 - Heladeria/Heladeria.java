import java.util.Scanner;

import Models.losCajeros;
import Models.losEnvases;
import Models.losGusto;
import Models.losHeladeros;
import Models.losPedidos;
import Models.losPlus;

public class Heladeria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Agregar-ALGO ingresa los datos pedidos en el enunciado

		losHeladeros.agregar(4);
		// losHeladeros.agregar(); Es para uno solo
		losEnvases.agregarEnvases();
		losCajeros.agregarCajeros();
		losGusto.agregarGustos();
		losPlus.agergarPlus();

		System.out.print("Ingrese la cantidad de Pedidos que desea regristrar: ");
		@SuppressWarnings("resource")
		int _nroPedidos = new Scanner(System.in).nextInt();

		while (losPedidos.totales() < _nroPedidos) {
			losPedidos.preparar();
			losHeladeros.tomarOrden(losCajeros.asignar(), losPedidos.ultimo());
		}
		losCajeros.mostrar();
		losHeladeros.informe();

		losHeladeros.informeDetallado();
		InformeTotal();

	}

	private static void InformeTotal() {
		System.out.println("ENVASES --------------------------------------------");
		System.out.println(losEnvases.mostrar());
		System.out.println("CAJEROS --------------------------------------------");
		System.out.println(losCajeros.mostrar());
		System.out.println("PEDIDOS --------------------------------------------");
		System.out.println(losPedidos.mostrar());
		System.out.println("GUSTOS --------------------------------------------");
		System.out.println(losGusto.mostrar());
	}

}
