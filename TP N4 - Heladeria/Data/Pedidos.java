package Data;

import java.util.ArrayList;

public class Pedidos {
	private String _envase;
	private ArrayList<Gustos> _gusto;
	private Plus _plus;
	
	public Pedidos(String envase, ArrayList<Gustos> _listGustos, Plus _plus) {
		this._envase = envase;
		this._gusto = _listGustos;
		this._plus = _plus;
	}

	@Override
	public String toString() {
		return " Envase: " + _envase + 
				"\n Gustos:" + _gusto + 
				"\n Plus:" + _plus +
				"\n ";
			
	}
	

}
