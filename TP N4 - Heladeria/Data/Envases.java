package Data;

public class Envases {
	private String _nombre;
	private int _cantGustos;
	private boolean _plus;

	public Envases(String nombre, int cantGusto, boolean cantPlus) {
		this._nombre = nombre;
		this._cantGustos = cantGusto;
		this._plus = cantPlus;
	}

	public Envases(String nombre, int cantGusto) {
		this._nombre = nombre;
		this._cantGustos = cantGusto;
		this._plus = false;
	}
	
	

	public String getNombre() {
		return _nombre;
	}

	public int getCantGustos() {
		return _cantGustos;
	}

	public boolean getPlus() {
		return _plus;
	}

	public void setNombre(String _nombre) {
		this._nombre = _nombre;
	}

	public void setCantGustos(int _cantGustos) {
		this._cantGustos = _cantGustos;
	}

	public void setPlus(boolean _cantPlus) {
		this._plus = _cantPlus;
	}

	@Override
	public String toString() {
		return " nombre: " + _nombre + 
				"\t cantGustos: " + _cantGustos + 
				"\t cantPlus: " + _plus +
				"\n";
	}
	
	

}
