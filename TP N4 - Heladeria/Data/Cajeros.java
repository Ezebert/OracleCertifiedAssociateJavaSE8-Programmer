package Data;

public class Cajeros {
	private int _pedidosHechos;
	private int _maxPedidos;

	public Cajeros(int maxPedidos) {
		// TODO Auto-generated constructor stub
		_pedidosHechos  = 0;
		_maxPedidos = maxPedidos;
	}
	

	public int getPedidosHechos() {
		return _pedidosHechos;
	}


	public int getMaxPedidos() {
		return _maxPedidos;
	}


	public void setPedidosHechos(int _pedidosHechos) {
		this._pedidosHechos = _pedidosHechos;
	}


	public void setMaxPedidos(int _maxPedidos) {
		this._maxPedidos = _maxPedidos;
	}


	@Override
	public String toString() {
		return " Pedidos Hechos: " + _pedidosHechos + 
				"\t maxPedidos: " + _maxPedidos 
				+" \n";
	}
	

}
