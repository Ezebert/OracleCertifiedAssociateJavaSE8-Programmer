public class doctor {
	private String resultado;
	private String causa;
	private String medicamentos;

	protected doctor(String resultado, String causa, String medicamentos) {
		this.resultado = resultado;
		this.causa = causa;
		this.medicamentos = medicamentos;
	}

	public String getResultado() {
		return resultado;
	}

	public String getCausa() {
		return causa;
	}

	public String getMedicamentos() {
		return medicamentos;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public void setCausa(String causa) {
		this.causa = causa;
	}

	public void setMedicamentos(String medicamentos) {
		this.medicamentos = medicamentos;
	}

	@Override
	public String toString() {
		return "\nDOCTOR: \n\t resultado:" + resultado + ", \n\t causa:" + causa + ", \n\t medicamentos:" + medicamentos
				+ "\n";
	}

}
