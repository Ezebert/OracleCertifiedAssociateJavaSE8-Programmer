
public class animal {
	private String nombre;
	private String tipo;
	private String raza;
	private int edad;
	private String causa;

	protected animal(String nombre, String tipo, String raza, int edad, String causa) {
		this.nombre = nombre;
		this.tipo = tipo;
		this.raza = raza;
		this.edad = edad;
		this.causa = causa;
	}

	public animal(String tipo) {
		// TODO Auto-generated constructor stub
		this.tipo = tipo;
		this.causa = "causa NULA;";
	}

	public animal() {
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public String getRaza() {
		return raza;
	}

	public int getEdad() {
		return edad;
	}

	public String getCausa() {
		return causa;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public void setCausa(String causa) {
		this.causa = causa;
	}
	
	@Override
	public String toString() {
		String ret ="ANIMAL \n\t nombre:" + nombre + "\n\t tipo:" + tipo + "\n\t raza:" + raza + "\n\t edad:" + edad + "\n\t causa:" + causa;
		return ret;			
	}

}
