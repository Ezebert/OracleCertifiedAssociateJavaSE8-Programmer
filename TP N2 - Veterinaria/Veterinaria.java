import java.util.ArrayList;
import java.util.Scanner;

public class Veterinaria {
	private static final Scanner sc = new Scanner(System.in);
	private final static String tipoAnimal[] = { "perro", "conejo", "gato" };

	private ArrayList<animal> _animal;
	private ArrayList<ArrayList<doctor>> _doctor;

	protected Veterinaria() {
		this._animal = new ArrayList<>();
		_doctor = new ArrayList<ArrayList<doctor>>();
		for (int i = 0; i < 5; i++)
			_doctor.add(new ArrayList<doctor>());
	}

	public void asignarDr(int i) {
		// TODO Auto-generated method stub
		int index = doctorLibre(i);
		System.out.print("[DOCTOR]: resultado: ");
		String resultado = sc.next();
		System.out.print("[DOCTOR]: medicamentos: ");
		String medicamentos = sc.next();
		this._doctor.get(index).add(new doctor(resultado, this._animal.get(this.animalesAtendidos()-1).getCausa(), medicamentos));
	}

	private int doctorLibre(int i) {
		// TODO Auto-generated method stub
		int index = i;
		while (true) {
			if (this._doctor.get(index).size() < 5) {
				return index;
			} else
				index = (int) (Math.random() * 5);
		}
	}

	private void agregarAnimal(String tipo) {
		// TODO Auto-generated method stub
		System.out.print("[ANIMAL]: nombre: ");
		String nombre = sc.next();
		System.out.print("[ANIMAL]: raza: ");
		String raza = sc.next();
		System.out.print("[ANIMAL]: edad: ");
		int edad = sc.nextInt();
		System.out.print("[ANIMAL]: causa: ");
		String causa = sc.next();
		this._animal.add(new animal(nombre, tipo, raza, edad, causa));
	}

	public int animalesAtendidos() {
		// TODO Auto-generated method stub
		return this._animal.size();
	}

	public void agregarPaciente(String tipo) {
		// TODO Auto-generated method stub
		agregarAnimal(tipo);
		asignarDr((int) (Math.random() * 5));
	}

	public boolean chequear(String tipo) {
		// TODO Auto-generated method stub
		boolean ret = false;
		for (String tipoAnimal : tipoAnimal) {
			if (tipo.toLowerCase().equals(tipoAnimal))
				return true;
		}
		return ret;
	}

	public void mostrarDoctor() {
		// TODO Auto-generated method stub
		String ret = "\n******************************************\n";
		for (int i = 0; i < 5; i++)
			ret += "Doctor Nro_" + i + " atendio a " + _doctor.get(i).size() + " animales\n";

		ret += "\n******************************************";
		System.out.println(ret);
	}

	@Override
	public String toString() {
		String ret = "";
		for (animal animal : _animal) {
			ret+= animal;
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < this._doctor.get(i).size(); j++) {
					if(this._doctor.get(i).get(j).getCausa().equals(animal.getCausa()))
						ret += this._doctor.get(i).get(j).toString();
				}
			}
			ret += "\n******************************************\n";
		}
		return ret;
	}
	

}
